Framework sample for api testing.
All tests were written for Restful-Booker service. 

Restful-Booker service description:
Restful-booker is a Create Read Update Delete Web API that comes with authentication features and 
loaded with a bunch of bugs for you to explore. The API comes pre-loaded with 10 records for you to 
work with and resets itself every 10 minutes back to that default state.

Detailed API documentation: http://restful-booker.herokuapp.com/apidoc/index.html

How to run:
 - locally:
       (you also have to install python, at least 3.9 version)
     - clone the repository to your local machine from https://gitlab.com/TimurN/demonstration-api:
     - type that command in your terminal: 
       - git clone git@gitlab.com:TimurN/demonstration-api.git
       - go to root directory of the project - /Demonstration_api
       - pip install -r requirements.txt (make sure you do it at the new environment)
       - pytest -v --check-max-tb=5 --alluredir=allure_results
       - allure serve allure_results
 - with the Docker:
        (you need to have docker installed):
      - type that command in your terminal: docker run --rm timurdockerov/demostration_api
  


