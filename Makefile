run_docker:
	docker build -t timurdockerov/demostration_api:latest .
	docker run --rm -e TEST_SCOPE="--check-max-tb=5" timurdockerov/demostration_api:latest

push_docker:
	docker push timurdockerov/demostration_api:latest

go:
	pytest -v --check-max-tb=5 --alluredir=allure_results

allure:
	allure serve allure_results
