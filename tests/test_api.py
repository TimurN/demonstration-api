import allure
import pytest
import pytest_check as soft_assert

from api.api_utils import send_create_booking, send_get_booking_id, send_partial_update_booking, send_update_booking
from api.client import BookingClient
from api.helpers import check_booking, clean_data_after_test, fake_booking, fake_date, fake_digit, fake_name, fake_word


# create token api


@pytest.mark.smoke
@pytest.mark.admin_user
@pytest.mark.create_token
@allure.description("positive scenario of admin login")
def test_correct_login():
    client = BookingClient("admin", "password123")
    response = client.create_token()
    token = response.json()["token"]
    soft_assert.is_true(response.status_code == 200)
    soft_assert.is_true(response.reason == "OK")
    soft_assert.is_true(isinstance(token, str))
    soft_assert.is_true(len(token) > 0)
    assert len(response.json()) == 1


@pytest.mark.skip(
    reason="unable to validate with incorrect credentials because " "it is not specified which credentials are correct"
)
@pytest.mark.create_token
@allure.description("admin login with invalid password")
def test_invalid_password_field_password_login():
    client = BookingClient("admin", "password132")
    response = client.create_token()
    soft_assert.is_true(response.status_code == 401)
    assert response.reason == "Bad credentials"


@pytest.mark.xfail(
    reason="unable to validate with incorrect credentials because " "it is not specified which credentials are correct"
)
@pytest.mark.create_token
@allure.description("admin login with invalid login")
def test_invalid_password_field_login():
    client = BookingClient("admin_1", "password123")
    response = client.create_token()
    soft_assert.is_true(response.status_code == 401)
    assert response.reason == "Bad credentials"


@pytest.mark.xfail(reason="bug")
@pytest.mark.create_token
@allure.description("admin login with incorrect username (empty)")
def test_empty_username_field_login():
    client = BookingClient(password="password123")
    response = client.create_token()
    soft_assert.is_true(response.status_code == 401)
    assert response.reason == "Bad credentials"


@pytest.mark.xfail(reason="bug")
@pytest.mark.create_token
@allure.description("admin login with incorrect password (empty)")
def test_empty_password_field_login():
    client = BookingClient(username="admin")
    response = client.create_token()
    soft_assert.is_true(response.status_code == 401)
    assert response.reason == "Bad credentials"


@pytest.mark.xfail(reason="bug")
@pytest.mark.create_token
@allure.description("admin login with both empty fields")
def test_empty_username_password_field_login():
    client = BookingClient()
    response = client.create_token()
    soft_assert.is_true(response.status_code == 401)
    assert response.reason == "Bad credentials"


@pytest.mark.xfail(reason="bug")
@pytest.mark.create_token
@allure.description("admin login with incorrect type username (int)")
def test_wrong_type_username_field_login():
    client = BookingClient(1, "password123")
    response = client.create_token()
    soft_assert.is_true(response.status_code == 401)
    assert response.reason == "Bad credentials"


@pytest.mark.xfail(reason="bug")
@pytest.mark.create_token
@allure.description("admin login with incorrect type password (int)")
def test_wrong_type_password_field_login():
    client = BookingClient("admin", 3)
    response = client.create_token()
    soft_assert.is_true(response.status_code == 401)
    assert response.reason == "Bad credentials"


@pytest.mark.xfail(reason="bug")
@pytest.mark.create_token
@allure.description("admin login with incorrect type header")
def test_wrong_header_value_login():
    client = BookingClient("admin", "password123")
    client.update_headers({"Cookie": "text/html"})
    response = client.create_token()
    soft_assert.is_true(response.status_code == 415)
    assert response.reason == "Unsupported Media Type"


# create booking api


@pytest.mark.smoke
@pytest.mark.unauthorized_user
@pytest.mark.create_booking
@allure.description("positive - creating a booking")
def test_create_booking(unauth_client, admin_client):
    id_list = []
    try:
        to_send = fake_booking()
        response = send_create_booking(unauth_client, *to_send.expand_attributes())
        id_list.append(response.bookingid)
        check_booking(response, to_send)
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.xfail(reason="bug")
@pytest.mark.unauthorized_user
@pytest.mark.create_booking
@allure.description("empty request payload")
def test_create_booking_empty_payload(unauth_client):
    response = unauth_client.create_booking(*[None] * 7)
    soft_assert.is_true(response.status_code == 400)
    assert response.reason == "Bad Request"


@pytest.mark.xfail(reason="bug")
@pytest.mark.unauthorized_user
@pytest.mark.create_booking
@allure.description("invalid content type header")
def test_create_booking_invalid_content_type_header(unauth_client):
    to_send = BookingClient()
    to_send.update_headers({"Content-Type": "text/html"})
    response = to_send.create_booking(*fake_booking().expand_attributes())
    soft_assert.is_true(response.status_code == 415)
    assert response.reason == "Unsupported Media Type"


@pytest.mark.xfail(reason="bug")
@pytest.mark.unauthorized_user
@pytest.mark.create_booking
@allure.description("invalid accept header")
def test_create_booking_invalid_accept_header(unauth_client):
    to_send = BookingClient()
    to_send.update_headers({"Accept": "text/html"})
    response = to_send.create_booking(*fake_booking().expand_attributes())
    soft_assert.is_true(response.status_code == 415)
    assert response.status_code == 415


# delete booking api


@pytest.mark.smoke
@pytest.mark.admin_user
@pytest.mark.delete_booking
@allure.description("positive scenario of deleting a booking by admin")
def test_delete_booking(admin_client):
    to_send = fake_booking()
    response_1 = send_create_booking(admin_client, *to_send.expand_attributes())
    bookingid = response_1.bookingid
    response_2 = admin_client.delete_booking(bookingid)
    assert response_2.status_code == 201
    assert response_2.reason == "Created"
    assert admin_client.get_booking_id(bookingid).status_code == 404


@pytest.mark.xfail(reason="bug")
@pytest.mark.admin_user
@pytest.mark.delete_booking
@allure.description("deleting a booking by admin with str type id")
def test_delete_booking_wrong_type_id(admin_client):
    to_send = fake_booking()
    response_1 = send_create_booking(admin_client, *to_send.expand_attributes())
    bookingid = response_1.bookingid
    response_2 = admin_client.delete_booking(str(bookingid))
    assert response_2.status_code == 400


@pytest.mark.xfail(reason="bug")
@pytest.mark.admin_user
@pytest.mark.delete_booking
@allure.description("deleting a booking, wich doesn't exist by admin")
def test_delete_booking_doesnt_exist(admin_client):
    fake_id = 123456789
    requests = admin_client.delete_booking(fake_id)
    assert requests.status_code == 404


@pytest.mark.delete_booking
@pytest.mark.admin_user
@allure.description("deleting a booking, sending empty id value")
def test_delete_booking_empty_value(admin_client):
    requests = admin_client.delete_booking("")
    assert requests.status_code == 404


@pytest.mark.unauthorized_user
@pytest.mark.delete_booking
@allure.description("unauthorized user deleting a booking")
def test_delete_booking_by_unauth_client(unauth_client):
    to_send = fake_booking()
    response_1 = send_create_booking(unauth_client, *to_send.expand_attributes())
    bookingid = response_1.bookingid
    response_2 = unauth_client.delete_booking(bookingid)
    assert response_2.status_code == 403


# get booking ids api


@pytest.mark.smoke
@pytest.mark.unauthorized_user
@pytest.mark.get_booking_ids
@allure.description("positive scenario of getting booking by info (getting all bookings)")
def test_get_booking_info_all_ids(unauth_client):
    response = unauth_client.get_booking_ids()
    soft_assert.is_true(response.status_code == 200)
    soft_assert.is_true(response.reason == "OK")
    soft_assert.is_true(isinstance(response.json(), list))
    soft_assert.is_true(len(response.json()) > 0)
    soft_assert.is_true("bookingid" in response.json()[0])
    assert isinstance(response.json()[0]["bookingid"], int)


@pytest.mark.smoke
@pytest.mark.unauthorized_user
@pytest.mark.get_booking_ids
@allure.description("positive scenario of getting booking by info (firstname)")
def test_get_booking_info_by_firstname(unauth_client, admin_client):
    id_list = []
    try:
        to_send_1 = fake_booking()
        to_send_2 = fake_booking()
        to_send_1.firstname = to_send_2.firstname = "random_firstname"
        response_1 = send_create_booking(unauth_client, *to_send_1.expand_attributes())
        id_list.append(response_1.bookingid)
        response_2 = send_create_booking(unauth_client, *to_send_2.expand_attributes())
        id_list.append(response_2.bookingid)
        response_3 = unauth_client.get_booking_ids(firstname="random_firstname")
        ids = [response_3.json()[i]["bookingid"] for i in range(len(response_3.json()))]
        soft_assert.is_true(response_3.status_code == 200)
        soft_assert.is_true(response_3.reason == "OK")
        soft_assert.is_true(response_3.json(), list)
        soft_assert.is_true(len(response_3.json()) == 2)
        soft_assert.is_true(response_1.bookingid != response_2.bookingid)
        soft_assert.is_true(response_1.bookingid in ids)
        assert response_2.bookingid in ids
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.smoke
@pytest.mark.unauthorized_user
@pytest.mark.get_booking_ids
@allure.description("positive scenario of getting booking by info (lastname)")
def test_get_booking_info_by_lastname(unauth_client, admin_client):
    id_list = []
    try:
        to_send_1 = fake_booking()
        to_send_2 = fake_booking()
        to_send_1.lastname = to_send_2.lastname = "random_lastname"
        response_1 = send_create_booking(unauth_client, *to_send_1.expand_attributes())
        id_list.append(response_1.bookingid)
        response_2 = send_create_booking(unauth_client, *to_send_2.expand_attributes())
        id_list.append(response_2.bookingid)
        response_3 = unauth_client.get_booking_ids(lastname="random_lastname")
        ids = [response_3.json()[i]["bookingid"] for i in range(len(response_3.json()))]
        soft_assert.is_true(response_3.status_code == 200)
        soft_assert.is_true(response_3.reason == "OK")
        soft_assert.is_true(isinstance(response_3.json(), list))
        soft_assert.is_true(len(response_3.json()) == 2)
        soft_assert.is_true(response_1.bookingid != response_2.bookingid)
        soft_assert.is_true(response_1.bookingid in ids)
        assert response_2.bookingid in ids
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.smoke
@pytest.mark.unauthorized_user
@pytest.mark.get_booking_ids
@allure.description("positive scenario of getting booking by info: firstname and lastname)")
def test_get_booking_info_by_firstname_and_lastname(unauth_client, admin_client):
    id_list = []
    try:
        to_send_1 = fake_booking()
        to_send_2 = fake_booking()
        to_send_1.firstname = to_send_2.firstname = "random_firstname"
        to_send_1.lastname = to_send_2.lastname = "random_lastname"
        response_1 = send_create_booking(unauth_client, *to_send_1.expand_attributes())
        id_list.append(response_1.bookingid)
        response_2 = send_create_booking(unauth_client, *to_send_2.expand_attributes())
        id_list.append(response_2.bookingid)
        response_3 = unauth_client.get_booking_ids(firstname="random_firstname", lastname="random_lastname")
        ids = [response_3.json()[i]["bookingid"] for i in range(len(response_3.json()))]
        soft_assert.is_true(response_3.status_code == 200)
        soft_assert.is_true(response_3.reason == "OK")
        soft_assert.is_true(isinstance(response_3.json(), list))
        soft_assert.is_true(len(response_3.json()) == 2)
        soft_assert.is_true(response_1.bookingid != response_2.bookingid)
        soft_assert.is_true(response_1.bookingid in ids)
        assert response_2.bookingid in ids
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.xfail(reason="bug")
@pytest.mark.smoke
@pytest.mark.unauthorized_user
@pytest.mark.get_booking_ids
@allure.description("positive scenario of getting booking by info checkin date")
def test_get_booking_info_by_checkin_date(unauth_client, admin_client):
    id_list = []
    try:
        to_send_1 = fake_booking()
        to_send_2 = fake_booking()
        to_send_1.checkin = to_send_2.checkin = "3000-02-02"
        response_1 = send_create_booking(unauth_client, *to_send_1.expand_attributes())
        id_list.append(response_1.bookingid)
        response_2 = send_create_booking(unauth_client, *to_send_2.expand_attributes())
        id_list.append(response_2.bookingid)
        response_3 = unauth_client.get_booking_ids(checkin="3000-02-02")
        ids = [response_3.json()[i]["bookingid"] for i in range(len(response_3.json()))]
        soft_assert.is_true(response_3.status_code == 200)
        soft_assert.is_true(response_3.reason == "OK")
        soft_assert.is_true(isinstance(response_3.json(), list))
        soft_assert.is_true(len(response_3.json()) == 2)
        soft_assert.is_true(response_1.bookingid != response_2.bookingid)
        soft_assert.is_true(response_1.bookingid in ids)
        soft_assert.is_true(response_2.bookingid in ids)
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.smoke
@pytest.mark.unauthorized_user
@pytest.mark.get_booking_ids
@pytest.mark.xfail(reason="in this case all ids are deleting , bug")
@allure.description("positive scenario of getting booking by info checkout date")
def test_get_booking_info_by_checkout_date(unauth_client, admin_client):
    id_list = []
    try:
        to_send_1 = fake_booking()
        to_send_2 = fake_booking()
        to_send_1.checkout = to_send_2.checkout = "3234-05-02"
        response_1 = send_create_booking(unauth_client, *to_send_1.expand_attributes())
        id_list.append(response_1.bookingid)
        response_2 = send_create_booking(unauth_client, *to_send_2.expand_attributes())
        id_list.append(response_2.bookingid)
        response_3 = unauth_client.get_booking_ids(checkout="3234-05-02")
        ids = [response_3.json()[i]["bookingid"] for i in range(len(response_3.json()))]
        soft_assert.is_true(response_3.status_code == 200)
        soft_assert.is_true(response_3.reason == "OK")
        soft_assert.is_true(isinstance(response_3.json(), list))
        soft_assert.is_true(len(response_3.json()) == 2)
        soft_assert.is_true(response_1.bookingid != response_2.bookingid)
        soft_assert.is_true(response_1.bookingid in ids)
        assert response_2.bookingid in ids
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.xfail(reason="bug")
@pytest.mark.smoke
@pytest.mark.unauthorized_user
@pytest.mark.get_booking_ids
@allure.description("positive scenario of getting booking by info: checkin and checkout dates")
def test_get_booking_info_by_checkin_checkout_date(unauth_client, admin_client):
    id_list = []
    try:
        to_send_1 = fake_booking()
        to_send_2 = fake_booking()
        to_send_1.checkin = to_send_2.checkin = "3001-02-02"
        to_send_1.checkout = to_send_2.checkout = "3001-05-02"
        response_1 = send_create_booking(unauth_client, *to_send_1.expand_attributes())
        id_list.append(response_1.bookingid)
        response_2 = send_create_booking(unauth_client, *to_send_2.expand_attributes())
        id_list.append(response_2.bookingid)
        response_3 = unauth_client.get_booking_ids(checkin="3001-02-02", checkout="3001-05-02")
        ids = [response_3.json()[i]["bookingid"] for i in range(len(response_3.json()))]
        soft_assert.is_true(response_3.status_code == 200)
        soft_assert.is_true(response_3.reason == "OK")
        soft_assert.is_true(isinstance(response_3.json(), list))
        soft_assert.is_true(len(response_3.json()) == 2)
        soft_assert.is_true(response_1.bookingid != response_2.bookingid)
        soft_assert.is_true(response_1.bookingid in ids)
        assert response_2.bookingid in ids
    finally:
        clean_data_after_test(admin_client, id_list)


# get booking api


@pytest.mark.smoke
@pytest.mark.unauthorized_user
@pytest.mark.get_booking_id
@allure.description("positive scenario of getting booking by id")
def test_get_booking_info_by_id(unauth_client, admin_client):
    id_list = []
    try:
        to_send = fake_booking()
        response_1 = send_create_booking(unauth_client, *to_send.expand_attributes())
        id_list.append(response_1.bookingid)
        response_2 = send_get_booking_id(unauth_client, response_1.bookingid)
        check_booking(response_2, to_send)
    finally:
        clean_data_after_test(admin_client, id_list)


# update booking api


@pytest.mark.smoke
@pytest.mark.admin_user
@pytest.mark.update_booking
@allure.description("positive scenario for updating a booking")
def test_updating_booking(admin_client):
    id_list = []
    try:
        to_send = fake_booking()
        response_1 = send_create_booking(admin_client, *to_send.expand_attributes())
        id_list.append(response_1.bookingid)
        response_2 = send_update_booking(admin_client, response_1.bookingid, *fake_booking().expand_attributes())
        assert response_2.firstname != to_send.firstname
        assert response_2.lastname != to_send.lastname
        assert response_2.totalprice != to_send.totalprice
        # assert response_2.depositpaid != to_send.depositpaid
        assert response_2.bookingdates["checkin"] != to_send.checkin
        assert response_2.bookingdates["checkout"] != to_send.checkout
        assert response_2.additionalneeds != to_send.additionalneeds
    finally:
        clean_data_after_test(admin_client, id_list)


# partial update booking api


@pytest.mark.unauthorized_user
@pytest.mark.partial_update_booking
@allure.description("unauthorized user updating a partial payload (firstname)")
def test_patch_booking_firstname_by_unauth_client(unauth_client, admin_client):
    id_list = []
    try:
        to_send = fake_booking()
        response_1 = send_create_booking(unauth_client, *to_send.expand_attributes())
        bookingid = response_1.bookingid
        id_list.append(bookingid)
        new_name = fake_name()
        response_2 = unauth_client.partial_update_booking(id=bookingid, firstname=new_name)
        assert response_2.status_code == 403
        assert response_2.text == "Forbidden"
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.smoke
@pytest.mark.admin_user
@pytest.mark.partial_update_booking
@allure.description("positive scenario of admin updating a partial payload (firstname)")
def test_patch_booking_firstname(admin_client):
    id_list = []
    try:
        to_send = fake_booking()
        response_1 = send_create_booking(admin_client, *to_send.expand_attributes())
        bookingid = response_1.bookingid
        id_list.append(bookingid)
        new_name = fake_name()
        response_2 = send_partial_update_booking(admin_client, id=bookingid, firstname=new_name)
        to_send.firstname = new_name
        check_booking(response_2, to_send)
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.smoke
@pytest.mark.admin_user
@pytest.mark.partial_update_booking
@allure.description("positive scenario of admin updating a partial payload (lastname)")
def test_patch_booking_lastname(admin_client):
    id_list = []
    try:
        to_send = fake_booking()
        response_1 = send_create_booking(admin_client, *to_send.expand_attributes())
        bookingid = response_1.bookingid
        id_list.append(bookingid)
        new_lastname = fake_name()
        response_2 = send_partial_update_booking(admin_client, id=bookingid, lastname=new_lastname)
        to_send.lastname = new_lastname
        check_booking(response_2, to_send)
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.smoke
@pytest.mark.admin_user
@pytest.mark.partial_update_booking
@allure.description("positive scenario of admin updating a partial payload (totalprice)")
def test_patch_booking_totalprice(admin_client):
    id_list = []
    try:
        to_send = fake_booking()
        response_1 = send_create_booking(admin_client, *to_send.expand_attributes())
        bookingid = response_1.bookingid
        id_list.append(bookingid)
        new_totalprice = fake_digit()
        response_2 = send_partial_update_booking(admin_client, id=bookingid, totalprice=new_totalprice)
        to_send.totalprice = new_totalprice
        check_booking(response_2, to_send)
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.smoke
@pytest.mark.admin_user
@pytest.mark.partial_update_booking
@allure.description("positive scenario of admin updating a partial payload (depositpaid)")
def test_patch_booking_depositpaid(admin_client):
    id_list = []
    try:
        to_send = fake_booking()
        response_1 = send_create_booking(admin_client, *to_send.expand_attributes())
        bookingid = response_1.bookingid
        id_list.append(bookingid)
        new_depositpaid = False if response_1.booking["depositpaid"] else True
        response_2 = send_partial_update_booking(admin_client, id=bookingid, depositpaid=new_depositpaid)
        to_send.depositpaid = new_depositpaid
        check_booking(response_2, to_send)
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.xfail(reason="bug")
@pytest.mark.smoke
@pytest.mark.admin_user
@pytest.mark.partial_update_booking
@allure.description("positive scenario of admin updating a partial payload (checkin)")
def test_patch_booking_checkin(admin_client):
    id_list = []
    try:
        to_send = fake_booking()
        response_1 = send_create_booking(admin_client, *to_send.expand_attributes())
        bookingid = response_1.bookingid
        id_list.append(bookingid)
        new_date = fake_date()
        response_2 = send_partial_update_booking(admin_client, id=bookingid, checkin=new_date)
        to_send.checkin = new_date
        check_booking(response_2, to_send)
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.xfail(reason="bug")
@pytest.mark.smoke
@pytest.mark.admin_user
@pytest.mark.partial_update_booking
@allure.description("positive scenario of admin updating a partial payload (checkout)")
def test_patch_booking_checkout(admin_client):
    id_list = []
    try:
        to_send = fake_booking()
        response_1 = send_create_booking(admin_client, *to_send.expand_attributes())
        bookingid = response_1.bookingid
        id_list.append(bookingid)
        new_date = fake_date()
        response_2 = send_partial_update_booking(admin_client, id=bookingid, checkout=new_date)
        to_send.checkout = new_date
        check_booking(response_2, to_send)
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.admin_user
@pytest.mark.partial_update_booking
@allure.description("updating a partial payload, checkout date earlier and checkin date)")
def test_patch_booking_checkin_checkout_incorrect_values(admin_client):
    id_list = []
    try:
        to_send = fake_booking()
        response_1 = send_create_booking(admin_client, *to_send.expand_attributes())
        bookingid = response_1.bookingid
        id_list.append(bookingid)
        new_date_1 = "2023-04-16"
        new_date_2 = "2023-04-15"
        response_2 = send_partial_update_booking(admin_client, id=bookingid, checkin=new_date_1, checkout=new_date_2)
        to_send.checkin = new_date_1
        to_send.checkout = new_date_2
        check_booking(response_2, to_send)
    finally:
        clean_data_after_test(admin_client, id_list)


@pytest.mark.smoke
@pytest.mark.admin_user
@pytest.mark.partial_update_booking
@allure.description("positive scenario of admin updating a partial payload (additionalneeds)")
def test_patch_booking_additionalneeds(admin_client):
    id_list = []
    try:
        to_send = fake_booking()
        response_1 = send_create_booking(admin_client, *to_send.expand_attributes())
        bookingid = response_1.bookingid
        id_list.append(bookingid)
        new_additionalneeds = fake_word()
        response_2 = send_partial_update_booking(admin_client, id=bookingid, additionalneeds=new_additionalneeds)
        to_send.additionalneeds = new_additionalneeds
        check_booking(response_2, to_send)
    finally:
        clean_data_after_test(admin_client, id_list)
