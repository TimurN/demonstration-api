import pytest

from api.client import BookingClient


@pytest.fixture
def admin_client():
    client = BookingClient("admin", "password123")
    client.set_token(client.create_token().json()["token"])
    yield client
    client.session.close()


@pytest.fixture
def unauth_client():
    client = BookingClient()
    yield client
    client.session.close()
