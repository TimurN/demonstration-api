# making a build
FROM python:3.11-slim
WORKDIR ./Demonstration-api/

COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY . .

# take a look at pytest.ini, when you will run a cointainer you can give a specific tests scope
#with TEST_SCOPE argument, for example like this: -e TEST_SCOPE='-m "smoke"'
ENV TEST_SCOPE ""

# run a container
CMD pytest -v $TEST_SCOPE --alluredir=allure_results .