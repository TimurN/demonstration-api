import logging

import allure
from requests import session

from api.helpers import clean_data


logger = logging.getLogger()


class BookingClient:
    def __init__(self, username: str = None, password: str = None):
        self.session = session()
        self.username = username
        self.password = password
        self.hostname = "https://restful-booker.herokuapp.com"
        self.token = None
        self.path = "/booking"

    def set_token(self, token):
        self.token = token
        self.session.headers.update({"Cookie": f"token={self.token}"})
        logger.info(f"\ntoken: {self.token}")

    def update_headers(self, header: dict):
        self.session.headers.update(header)

    # endpoints
    def create_token(self):
        path = "/auth"
        data = {"username": self.username, "password": self.password}
        data = clean_data(data)
        logger.info(f"\nsending create token request with data: {data}")
        response = self.session.post(self.hostname + path, json=data)
        logger.info(f"\nresponse of create token request: " f"{response.status_code, response.reason}")
        return response

    def get_booking_ids(
        self,
        firstname: str = None,
        lastname: str = None,
        checkin: str = None,
        checkout: str = None,
    ):
        """
        Returns the ids of all the bookings that exist within the API.
        Can take optional query
        strings to search and return a subset of booking ids
        """
        parametrs = {
            "firstname": firstname,
            "lastname": lastname,
            "checkin": checkin,
            "checkout": checkout,
        }

        parametrs = clean_data(parametrs)
        logger.info(f"\nsending get booking ids request with params: {parametrs}")
        response = self.session.get(self.hostname + self.path, params=parametrs)
        logger.info(f"\nget booking ids request id list: {response.json()}")
        logger.info(f"\nresponse of get booking ids request: " f"{response.status_code, response.reason}")
        return response

    def get_booking_id(self, id: str):
        """
        Returns a specific booking based upon the booking id provided
        """
        logger.info(f"\nsending get booking id request with param: {id}")
        response = self.session.get(self.hostname + self.path + f"/{id}")
        logger.info(f"\nresponse of get booking id request: " f"{response.status_code, response.reason}")
        return response

    def create_booking(
        self,
        firstname: str,
        lastname: str,
        totalprice: int,
        depositpaid: bool,
        checkin: str,
        checkout: str,
        additionalneeds: str,
    ):
        """
        Creates a new booking in the API
        """
        data = {
            "firstname": firstname,
            "lastname": lastname,
            "totalprice": totalprice,
            "depositpaid": depositpaid,
            "bookingdates": {"checkin": checkin, "checkout": checkout},
            "additionalneeds": additionalneeds,
        }
        data = clean_data(data)
        logger.info(f"\nsending create booking request with data: {data}")
        response = self.session.post(self.hostname + self.path, json=data)
        logger.info(f"\nresponse of create booking request: " f"{response.status_code, response.reason}")
        return response

    @allure.step("sending update booking request")
    def update_booking(
        self,
        id: int,
        firstname: str,
        lastname: str,
        totalprice: int,
        depositpaid: bool,
        checkin: str,
        checkout: str,
        additionalneeds: str,
    ):
        """
        Updates a current booking
        """
        data = {
            "firstname": firstname,
            "lastname": lastname,
            "totalprice": totalprice,
            "depositpaid": depositpaid,
            "bookingdates": {"checkin": checkin, "checkout": checkout},
            "additionalneeds": additionalneeds,
        }
        data = clean_data(data)
        logger.info(f"\nsending update booking request with data: {data}")
        response = self.session.put(self.hostname + self.path + f"/{id}", json=data)
        logger.info(f"\nresponse of update booking request: " f"{response.status_code, response.reason}")
        return response

    def partial_update_booking(
        self,
        id: int,
        firstname: str = None,
        lastname: str = None,
        totalprice: str = None,
        depositpaid: str = None,
        checkin: str = None,
        checkout: str = None,
        additionalneeds: str = None,
    ):
        """
        Updates a current booking with a partial payload
        """
        data = {
            "firstname": firstname,
            "lastname": lastname,
            "totalprice": totalprice,
            "depositpaid": depositpaid,
            "bookingdates": {"checkin": checkin, "checkout": checkout},
            "additionalneeds": additionalneeds,
        }
        data = clean_data(data)
        logger.info(f"\nsending partial update booking request with data: {data}")
        response = self.session.patch(self.hostname + self.path + f"/{id}", json=data)
        logger.info(f"\nresponse of partial update booking request: " f"{response.status_code, response.reason}")
        return response

    def delete_booking(self, id: int):
        """
        Returns the ids of all the bookings that exist within the API.
        Can take optional query
        strings to search and return a subset of booking ids
        """
        logger.info(f"\nsending delete booking request with id: {id}")
        response = self.session.delete(self.hostname + self.path + f"/{id}")
        logger.info(f"\nresponse of delete booking request: " f"{response.status_code, response.reason}")
        return response
