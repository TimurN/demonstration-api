import logging

import pytest
from faker import Faker

from api.models import Booking


fake = Faker()
logger = logging.getLogger()


def clean_data(data):
    """
    Delete all keys where value is None
    """
    for key in data.copy():
        if isinstance(data[key], dict):
            for i in data[key].copy():
                if set([data[key][i] for i in data[key]]) == {None}:
                    del data[key]
                    break
                elif data[key][i] is None:
                    del data[key][i]
        elif data[key] is None:
            del data[key]
    return data


def fake_booking():
    """
    Create a Booking() object
    """
    booking = Booking(
        fake.first_name(),
        fake.last_name(),
        fake.pyint(),
        fake.pybool(),
        fake.date(),
        fake.date(),
        fake.word(),
    )
    return booking


def fake_name():
    """
    Generate a fake name
    """
    return fake.first_name()


def fake_digit():
    """
    Generate a fake digit
    """
    return fake.pyint()


def fake_date():
    """
    Generate a fake date
    """
    return fake.date()


def fake_word():
    """
    Generate a fake word
    """
    return fake.word()


def fake_id():
    """
    Generate a fake id
    """
    id = fake.unique.random_int(min=99999, max=999999)
    return id


def check_booking(response, to_send):
    """
    Compare the sent values with the values that were received
    """
    logger.info(
        f"\nCompare the sent values with the values that were received:"
        f"\nSended object{to_send.__dict__}"
        f"\nObject of response{response.__dict__}"
    )
    try:
        if getattr(response, "bookingid"):
            assert isinstance(response.bookingid, int)
            assert response.booking["firstname"] == to_send.firstname, (
                "response.booking['firstname']: " f"{response.booking['firstname']} == " f"{to_send.firstname}"
            )
            assert response.booking["lastname"] == to_send.lastname, (
                "response.booking['lastname']:" f"{response.booking['lastname']} == " f"{to_send.lastname}"
            )
            assert response.booking["totalprice"] == to_send.totalprice, (
                "response.booking['totalprice']: " f"{response.booking['totalprice']} == " f"{to_send.totalprice}"
            )
            assert response.booking["depositpaid"] == to_send.depositpaid, (
                "response.booking['depositpaid']: " f"{response.booking['depositpaid']} == " f"{to_send.depositpaid}"
            )
            assert response.booking["bookingdates"]["checkin"] == to_send.checkin, (
                "booking['bookingdates']['checkin']: "
                f"{response.booking['bookingdates']['checkin']} == "
                f"{to_send.checkin}",
            )
            assert response.booking["bookingdates"]["checkout"] == to_send.checkout, (
                "booking['bookingdates']['checkout']: "
                "{response.booking['bookingdates']['checkout']} == "
                "{to_send.checkout}"
            )

            assert response.booking["additionalneeds"] == to_send.additionalneeds, (
                "response.booking['additionalneeds']: "
                "{response.booking['additionalneeds']} == "
                f"{to_send.additionalneeds}"
            )

    except AttributeError:
        assert response.firstname == to_send.firstname, (
            f"response.firstname: {response.firstname} == " f"{to_send.firstname}"
        )
        assert response.lastname == to_send.lastname, (
            f"response.lastname: {response.lastname} == " f"{to_send.lastname}e"
        )
        assert response.totalprice == to_send.totalprice, (
            f"response.totalprice: {response.totalprice} == " f"{to_send.totalprice}"
        )
        assert response.depositpaid == to_send.depositpaid, (
            f"response.depositpaid: {response.depositpaid} == " f"{to_send.depositpaid}"
        )
        assert response.bookingdates["checkin"] == to_send.checkin, (
            "response.bookingdates['checkin']: " f"{response.bookingdates['checkin']} == " f"{to_send.checkin}"
        )
        assert response.bookingdates["checkout"] == to_send.checkout, (
            "response.bookingdates['checkout']: " f"{response.bookingdates['checkout']} == " f"{to_send.checkout}"
        )
        assert response.additionalneeds == to_send.additionalneeds, (
            "response.additionalneeds: " f"{response.additionalneeds} == " f"{to_send.additionalneeds}"
        )


def clean_data_after_test(client, created_data: [list, str]):
    """
    Delete bookings which was created during the test
    """
    logger.info(f"\nids of created booking during the test: {created_data}")
    if isinstance(created_data, list):
        for i in reversed(created_data):
            try:
                response = client.delete_booking(i)
                assert response.status_code == 201
                created_data.remove(i)
            except AssertionError:
                continue
        if created_data:
            pytest.fail(f"failed deleting booking with id(s): {created_data}")
    else:
        client.delete_booking(created_data)
