from api.client import BookingClient
from api.models import ResponseToClass


def send_get_booking_ids(
    client: BookingClient,
    firstname: str = None,
    lastname: str = None,
    checkin: str = None,
    checkout: str = None,
):
    response = client.get_booking_ids(firstname, lastname, checkin, checkout)
    assert response.status_code == 200, f"status_code: {response.status_code} == 200"
    return response


def send_get_booking_id(client: BookingClient, id: str):
    response = client.get_booking_id(id)
    assert response.status_code == 200, f"status_code: {response.status_code} == 200"
    return ResponseToClass(response.json())


def send_create_booking(
    client: BookingClient,
    firstname: str,
    lastname: str,
    totalprice: int,
    depositpaid: bool,
    checkin: str,
    checkout: str,
    additionalneeds: str,
):
    response = client.create_booking(
        firstname,
        lastname,
        totalprice,
        depositpaid,
        checkin,
        checkout,
        additionalneeds,
    )
    assert response.status_code == 200, f"status_code: {response.status_code} == 200"
    return ResponseToClass(response.json())


def send_update_booking(
    client: BookingClient,
    id: int,
    firstname: str,
    lastname: str,
    totalprice: int,
    depositpaid: bool,
    checkin: str,
    checkout: str,
    additionalneeds: str,
):
    response = client.update_booking(
        id,
        firstname,
        lastname,
        totalprice,
        depositpaid,
        checkin,
        checkout,
        additionalneeds,
    )
    assert response.status_code == 200, f"status_code: {response.status_code} == 200"
    return ResponseToClass(response.json())


def send_partial_update_booking(
    client: BookingClient,
    id: int,
    firstname: str = None,
    lastname: str = None,
    totalprice: str = None,
    depositpaid: bool = None,
    checkin: str = None,
    checkout: str = None,
    additionalneeds: str = None,
):
    response = client.partial_update_booking(
        id,
        firstname,
        lastname,
        totalprice,
        depositpaid,
        checkin,
        checkout,
        additionalneeds,
    )
    assert response.status_code == 200, f"status_code: {response.status_code} == 200"
    return ResponseToClass(response.json())


def send_delete_booking(client: BookingClient, id: int):
    response = client.delete_booking(id)

    assert response.status_code == 201, f"status_code: {response.status_code} == 201"
    return ResponseToClass(response.json())
