class Booking:
    def __init__(
        self,
        firstname: str,
        lastname: str,
        totalprice: int,
        depositpaid: bool,
        checkin: str,
        checkout: str,
        additionalneeds: str,
    ):
        self.firstname = firstname
        self.lastname = lastname
        self.totalprice = totalprice
        self.depositpaid = depositpaid
        self.checkin = checkin
        self.checkout = checkout
        self.additionalneeds = additionalneeds

    def expand_attributes(self):
        return self.__dict__.values()


class ResponseToClass(object):
    def __init__(self, my_dict):
        for key in my_dict:
            setattr(self, key, my_dict[key])
